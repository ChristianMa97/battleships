package de.hdm_stuttgart.mi.se2.core.players;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BsComputerPlayerTest {

    private static Logger log = LogManager.getLogger(BsHumanPlayer.class);

    @Test
    public void createPlayer() {
        BsComputerPlayer computerPlayer = new BsComputerPlayer("Computer", (byte) 12);

        assertEquals("Player name is correct", "Computer", computerPlayer.getPlayerName());
        assertEquals("Grid size is correct", (byte) 12, computerPlayer.grid.getSize());
        log.info("Constructor works, player name and grid size are correct");
    }
}
