package de.hdm_stuttgart.mi.se2.core.grid;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        BsCoordinateTest.class,
        BsFieldTest.class,
        BsGridTest.class
})

public class gridTests {

}
