package de.hdm_stuttgart.mi.se2.core.ships;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        IBsShipFactoryTest.class,
        BsShipTest.class,
        BsShipCollectionTest.class
})

public class shipsTests {
}
