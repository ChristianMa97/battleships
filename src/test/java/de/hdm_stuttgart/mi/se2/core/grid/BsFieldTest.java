package de.hdm_stuttgart.mi.se2.core.grid;

import de.hdm_stuttgart.mi.se2.core.ships.BsShipFactory;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidAttackException;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.*;

public class BsFieldTest {

    private static Logger log = LogManager.getLogger(BsFieldTest.class);

    private static IBsShip ship;

    static {
        try {
            ship = new BsShipFactory().createShip(IBsShip.ShipType.Destroyer);
        } catch (IllegalFactoryArgument illegalFactoryArgument) {
            illegalFactoryArgument.printStackTrace();
        }
    }

    @Test
    public void attack() {
        log.info("TEST: field.attack():");
        boolean eThrown = false;
        BsField f1 = new BsField();             //empty field
        BsField f2 = new BsField(ship, false);         //field with ship
        BsField f3 = new BsField(null, true);//empty field that has been attacked previously
        BsField f4 = new BsField(ship, false);         //field with the same ship again so it can be hit more than ocne

        try {
            assertFalse(f1.attack());
            assertTrue(f2.attack());
        } catch (InvalidAttackException e) {
            eThrown = true;
            log.debug("Caught InvalidAttackException: " + e.getMessage());

        }
        assertFalse(eThrown);

        try {
            f3.attack();
        } catch (InvalidAttackException e) {
            eThrown = true;
            log.debug("Caught InvalidAttackException: " + e.getMessage());
        }
        assertTrue(eThrown);

        eThrown = false;
        try {
            f4.attack();
        } catch (InvalidAttackException e) {
            eThrown = true;
            log.debug("Caught InvalidAttackException: " + e.getMessage());
        }
        assertTrue(eThrown);


        log.info("field.attack - OK");
    }

    @Test
    public void placeShip() {
        log.info("TEST: field.placeShip():");
        boolean eThrown = false;
        BsField f1 = new BsField();
        BsField f2 = new BsField(ship, false);

        try {
            f1.placeShip(ship);
        } catch (InvalidPlacementException e) {
            eThrown = true;
            log.debug("Caught InvalidPlacementException: " + e.getMessage());
        }
        assertFalse(eThrown);

        eThrown = false;
        try {
            f2.placeShip(ship);
        } catch (InvalidPlacementException e) {
            eThrown = true;
            log.debug("Caught InvalidPlacementException: " + e.getMessage());
        }
        assertTrue(eThrown);
        log.info("DONE: field.placeShip - OK");
    }

    @Test
    public void isAttacked() {
        log.info("TEST: field.isAttacked():");
        BsField f1 = new BsField();
        BsField f2 = new BsField(null, true);

        assertFalse(f1.isAttacked());
        assertTrue(f2.isAttacked());
        log.info("DONE: field.isAttacked - OK");
    }

    @Test
    public void isOccupied() {
        log.info("TEST: field.isOccupied():");
        BsField f1 = new BsField();
        BsField f2 = new BsField(ship, false);

        assertFalse(f1.isOccupied());
        assertTrue(f2.isOccupied());

        assertNull(f1.getShip());
        assertNotNull(f2.getShip());

        log.info("DONE: field.isOccupied - OK");
    }

    @Test
    public void removeShip() throws IllegalFactoryArgument {
        log.info("TEST: field.removeShip():");

        BsField f1 = new BsField(ship, false);
        BsField f2 = new BsField(new BsShipFactory().createShip(IBsShip.ShipType.Carrier), false);

        f1.removeShip(ship); //Should be the same ship -> should be removed
        f2.removeShip(ship); //Should be a different ship -> shouldn't be removed

        assertFalse(f1.isOccupied());
        assertTrue(f2.isOccupied());

        f2.removeShip();

        assertFalse(f2.isOccupied());

        log.info("DONE: field.removeShip - OK");
    }

    @Test
    public void getShip() {
        log.info("TEST: field.getShip():");
        BsField f1 = new BsField();
        BsField f2 = new BsField(ship, false);

        assertNull(f1.getShip());
        assertEquals(ship, f2.getShip());
        log.info("DONE: field.getShip - OK");
    }
}