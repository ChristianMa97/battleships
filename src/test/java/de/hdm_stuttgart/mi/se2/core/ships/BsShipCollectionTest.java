package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class BsShipCollectionTest {

    private static BsShipFactory wharf;

    private BsShipCollection collection;
    private static Logger log = LogManager.getLogger(BsShipCollectionTest.class);

    @BeforeClass
    public static void setUp() {
        wharf = new BsShipFactory();
    }

    @Test
    public void addShip() throws IllegalFactoryArgument {
        log.info("TEST: collection.addShip():");

        log.debug("Creating empty collection...");
        collection = new BsShipCollection(null);

        log.trace("Verifying it is empty..");
        assertTrue(collection.getShipList().isEmpty());

        log.debug("Creating a new Ship of type Carrier...");
        IBsShip ship = wharf.createShip(IBsShip.ShipType.Carrier);

        log.debug("Calling addShip to add Carrier to the list...");
        collection.addShip(ship);

        log.debug("Verifying that the ship exists in the List...");
        assertTrue(collection.contains(ship));

        log.info("DONE: collection.addShip - OK");
    }

    @Test
    public void createShip() throws IllegalFactoryArgument {
        log.info("TEST: collection.createShip():");

        log.debug("Creating collection...");
        collection = new BsShipCollection(null);

        log.trace("Ensuring the ship list is empty, as intended");
        assertTrue(collection.getShipList().isEmpty());

        log.debug("Calling createShip()...");
        IBsShip ship = collection.createShip(IBsShip.ShipType.Destroyer);

        log.trace("Checking that the list is no longer empty");
        assertNotEquals(0, collection.getShipList().size());

        log.debug("Checking if collection contains ship...");
        assertTrue(collection.contains(ship));

        log.debug("Checking if ship is of correct type");
        assertEquals(IBsShip.ShipType.Destroyer, ship.getType());

        log.info("DONE: collection.createShip - OK");
    }
}