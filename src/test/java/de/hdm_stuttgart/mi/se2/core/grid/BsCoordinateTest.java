package de.hdm_stuttgart.mi.se2.core.grid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.*;

public class BsCoordinateTest {

    private static Logger log = LogManager.getLogger(BsCoordinateTest.class);

    @Test
    public void testHashCode() {
        log.info("TEST: coordinate.hashCode():");
        BsCoordinate bsc1 = new BsCoordinate((byte) 1, (byte) 2);
        BsCoordinate bsc2 = new BsCoordinate((byte) 2, (byte) 1);
        BsCoordinate bsc3 = new BsCoordinate((byte) 3, (byte) 3);

        assertEquals(1, bsc1.hashCode());
        assertEquals(2, bsc2.hashCode());
        assertEquals(3, bsc3.hashCode());
        log.info("DONE: coordinate.hashCode - OK");
    }

    @Test
    public void testEquals() {
        log.info("TEST: coordinate.equals():");
        BsCoordinate bsc1 = new BsCoordinate((byte) 1, (byte) 1);
        BsCoordinate bsc2 = new BsCoordinate((byte) 1, (byte) 1);
        BsCoordinate bsc3 = new BsCoordinate((byte) 1, (byte) 2);
        Integer integer = 2;

        assertTrue(bsc1.equals(bsc2));
        assertFalse(bsc1.equals(bsc3));
        assertFalse(bsc1.equals(integer));
        log.info("DONE: coordinate.equals - OK");
    }

    @Test
    public void testToString() {
        log.info("TEST: coordinate.toString():");
        BsCoordinate bsc1 = new BsCoordinate((byte) 1, (byte) 2);
        BsCoordinate bsc2 = new BsCoordinate((byte) 2, (byte) 1);
        BsCoordinate bsc3 = new BsCoordinate((byte) 3, (byte) 3);

        assertEquals("(1|2)", bsc1.toString());
        assertEquals("(2|1)", bsc2.toString());
        assertEquals("(3|3)", bsc3.toString());
        log.info("DONE: coordinate.toString - OK");
    }

    @Test
    public void testToArray() {
        log.info("TEST: coordinate.toArray():");
        BsCoordinate bsc1 = new BsCoordinate((byte) 1, (byte) 2);
        BsCoordinate bsc2 = new BsCoordinate((byte) 2, (byte) 1);
        BsCoordinate bsc3 = new BsCoordinate((byte) 3, (byte) 3);

        assertArrayEquals(bsc1.toArray(), new byte[]{1, 2});
        assertArrayEquals(bsc2.toArray(), new byte[]{2, 1});
        assertArrayEquals(bsc3.toArray(), new byte[]{3, 3});
        log.info("DONE: coordinate.toArray - OK");
    }
}