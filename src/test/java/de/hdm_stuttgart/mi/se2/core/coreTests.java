package de.hdm_stuttgart.mi.se2.core;

import de.hdm_stuttgart.mi.se2.core.grid.gridTests;
import de.hdm_stuttgart.mi.se2.core.ships.shipsTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        gridTests.class,
        shipsTests.class
})

public class coreTests {
}
