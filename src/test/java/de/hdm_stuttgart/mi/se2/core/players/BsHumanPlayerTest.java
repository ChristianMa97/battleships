package de.hdm_stuttgart.mi.se2.core.players;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.*;

public class BsHumanPlayerTest {

    private static Logger log = LogManager.getLogger(BsHumanPlayer.class);

    @Test
    public void createPlayer() {
        BsHumanPlayer humanPlayer = new BsHumanPlayer("Max", (byte) 12);

        assertEquals("Player name is correct", "Max", humanPlayer.getPlayerName());
        assertEquals("Grid size is correct", (byte) 12, humanPlayer.grid.getSize());
        log.info("Constructor works, player name and grid size are correct");
    }
}
