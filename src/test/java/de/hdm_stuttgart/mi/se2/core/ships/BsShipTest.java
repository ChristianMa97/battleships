package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import de.hdm_stuttgart.mi.se2.exceptions.ShipDeadException;
import junit.framework.AssertionFailedError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class BsShipTest {

    static BsShipFactory wharf;
    private static Logger log = LogManager.getLogger(BsShipTest.class);
    IBsShip ship;

    @BeforeClass
    public static void setUp() {
        log.debug("Creating Factory");
        wharf = new BsShipFactory();
    }

    @Before
    public void reset() throws Exception {
        log.debug("Resetting ship");
        ship = wharf.createShip(IBsShip.ShipType.Carrier);
    }

    @Test
    public void getType() {
        log.info("TEST: ship.getType():");
        assertEquals(ship.getType(), IBsShip.ShipType.Carrier);
        log.info("DONE: ship.getType - OK");
    }

    @Test
    public void hit() {
        log.info("TEST: ship.hit():");
        byte shipHealth = ship.getHealth();
        logShipHealth();

        try {
            log.trace("Hitting ship");
            ship.hit();
            logShipHealth();
            assertEquals(ship.getHealth(), shipHealth - 1);
            log.debug("ship.hit reduces health");

            ship.hit();
            ship.hit();
            ship.hit();
        } catch (ShipDeadException e) {
            throw new AssertionFailedError("Ship has died prematurely");
        }
        boolean failed = false;
        try {
            ship.hit();
        } catch (ShipDeadException e) {
            failed = true;
        }

        assertTrue("ShipDeadException thrown when attempting to hit a dead ship", failed);
        log.debug("ship.hit throws exception when attempting to hit a dead ship");

        log.info("DONE: ship.hit - OK");
    }

    @Test
    public void isDead() {
        log.info("TEST: ship.isDead():");
        logShipHealth();
        log.debug("Reducing ship's health to zero...");

        try {
            for (int i = 0; i < 4; i++) {
                assertFalse(ship.isDead());
                ship.hit();
            }
        } catch (ShipDeadException e) {
            throw new AssertionFailedError("Ship has died prematurely");
        }

        log.info("Ship's health reduced to zero. Is it dead?");
        logShipHealth();

        assertTrue(ship.isDead());
        log.info("DONE: ship.isDead - OK");
    }

    @Test
    public void place() throws InvalidPlacementException {
        log.info("TEST: ship.place(), ship.isPlaced() and ship.remove()");

        assertFalse(ship.isPlaced());
        log.debug("Default state is not placed");

        log.debug("Placing ship...");
        ship.place(); //Exception can't be thrown: The previous assertion ensures it isn't placed yet

        assertTrue("Ship is placed", ship.isPlaced());
        log.debug("Ship is placed");

        log.debug("Removing ship...");
        ship.remove();

        assertFalse(ship.isPlaced());
        log.info("DONE: ship.place, ship.isPlaced, ship.remove - OK");
    }

    public void logShipHealth() {
        log.debug("ShipHealth: " + ship.getHealth());
    }


}