package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class IBsShipFactoryTest {
    static BsShipFactory wharf;
    private static Logger log = LogManager.getLogger(IBsShipFactoryTest.class);

    @BeforeClass
    public static void setUp() throws Exception {
        wharf = new BsShipFactory();
    }

    @Test
    public void createCarrier() throws IllegalFactoryArgument {
        log.info("Creating and checking Carrier");
        IBsShip ship = wharf.createShip(IBsShip.ShipType.Carrier);
        checkShip(ship, IBsShip.ShipType.Carrier);
    }

    @Test
    public void createBattleship() throws IllegalFactoryArgument {
        log.info("Creating and checking Battleship");
        IBsShip ship = wharf.createShip(IBsShip.ShipType.Battleship);
        checkShip(ship, IBsShip.ShipType.Battleship);
    }

    @Test
    public void createCruiser() throws IllegalFactoryArgument {
        log.info("Creating and checking Cruiser");
        IBsShip ship = wharf.createShip(IBsShip.ShipType.Cruiser);
        checkShip(ship, IBsShip.ShipType.Cruiser);
    }

    @Test
    public void createDestroyer() throws IllegalFactoryArgument {
        log.info("Creating and checking Destroyer");
        IBsShip ship = wharf.createShip(IBsShip.ShipType.Destroyer);
        checkShip(ship, IBsShip.ShipType.Destroyer);
    }

    public void checkShip(IBsShip ship, IBsShip.ShipType type) {
        assertNotNull(ship);
        log.debug("NotNull - OK");

        assertEquals(ship.getType(), type);
        log.debug("Type - OK");

        byte expectedLength;
        switch (type) {
            case Carrier:
                expectedLength = 4;
                break;
            case Battleship:
                expectedLength = 3;
                break;
            case Cruiser:
                expectedLength = 2;
                break;
            case Destroyer:
                expectedLength = 1;
                break;
            default:
                expectedLength = 0;
        }


        assertEquals(ship.getLength(), expectedLength);
        log.debug("Length - OK");

        assertEquals(ship.getHealth(), expectedLength);
        log.debug("Health - OK");

        assertFalse(ship.isPlaced());
        log.debug("Not placed - OK");


    }
}