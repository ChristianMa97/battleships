package de.hdm_stuttgart.mi.se2.core.grid;

import de.hdm_stuttgart.mi.se2.core.ships.BsShipFactory;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidAttackException;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class BsGridTest {

    private static Logger log = LogManager.getLogger(BsGridTest.class);
    private static BsShipFactory wharf;

    private BsGrid grid;

    private BsCoordinate bsc00 = new BsCoordinate((byte) 0, (byte) 0);
    private BsCoordinate bsc11 = new BsCoordinate((byte) 1, (byte) 1);
    private BsCoordinate bsc44 = new BsCoordinate((byte) 4, (byte) 4);
    private BsCoordinate bsc46 = new BsCoordinate((byte) 4, (byte) 6);
    private BsCoordinate bsc64 = new BsCoordinate((byte) 6, (byte) 4);
    private BsCoordinate bsc66 = new BsCoordinate((byte) 6, (byte) 6);
    private BsCoordinate bsc88 = new BsCoordinate((byte) 8, (byte) 8);
    private BsCoordinate bscmm = new BsCoordinate((byte)-1, (byte)-1);

    @BeforeClass
    public static void setUp() {
        wharf = new BsShipFactory();
    }

    @Before
    public void init() {
        grid = new BsGrid((byte) 8);
    }

    @Test
    public void checkPlacement() throws IllegalFactoryArgument {
        log.info("TEST: grid.checkPlacement():");

        IBsShip carrier = wharf.createShip(IBsShip.ShipType.Carrier);

        assertTrue(grid.checkPlacement(bsc00, carrier, false));
        assertTrue(grid.checkPlacement(bsc00, carrier, true));
        assertTrue(grid.checkPlacement(bsc00, carrier.getLength(), false));
        assertTrue(grid.checkPlacement(bsc00, carrier.getLength(), true));

        assertTrue(grid.checkPlacement(bsc44, carrier, false));
        assertTrue(grid.checkPlacement(bsc44, carrier, true));
        assertTrue(grid.checkPlacement(bsc44, carrier.getLength(), false));
        assertTrue(grid.checkPlacement(bsc44, carrier.getLength(), true));

        assertTrue(grid.checkPlacement(bsc46, carrier, false));
        assertFalse(grid.checkPlacement(bsc46, carrier, true));
        assertTrue(grid.checkPlacement(bsc46, carrier.getLength(), false));
        assertFalse(grid.checkPlacement(bsc46, carrier.getLength(), true));

        assertFalse(grid.checkPlacement(bsc64, carrier, false));
        assertTrue(grid.checkPlacement(bsc64, carrier, true));
        assertFalse(grid.checkPlacement(bsc64, carrier.getLength(), false));
        assertTrue(grid.checkPlacement(bsc64, carrier.getLength(), true));

        assertFalse(grid.checkPlacement(bsc66, carrier, false));
        assertFalse(grid.checkPlacement(bsc66, carrier, true));
        assertFalse(grid.checkPlacement(bsc66, carrier.getLength(), false));
        assertFalse(grid.checkPlacement(bsc66, carrier.getLength(), true));

        boolean eThrown = false;
        try {
            grid.checkPlacement(bsc88, carrier, true);
        } catch (IllegalArgumentException e) {
            eThrown = true;
        }
        assertTrue(eThrown);

        eThrown = false;
        try {
            grid.checkPlacement(bscmm, carrier, true);
        } catch (IllegalArgumentException e) {
            eThrown = true;
        }
        assertTrue(eThrown);
        log.info("DONE: grid.checkPlacement - OK");
    }


    @Test
    public void getSize() {
        log.info("TEST: grid.getSize():");

        byte size6 = 6;
        BsGrid grid6 = new BsGrid(size6);
        assertEquals(size6, grid6.getSize());

        byte size8 = 8;
        BsGrid grid8 = new BsGrid(size8);
        assertEquals(size8, grid8.getSize());

        byte size10 = 10;
        BsGrid grid10 = new BsGrid(size10);
        assertEquals(size10, grid10.getSize());

        log.info("DONE: grid.getSize - OK");
    }

    @Test
    public void getShipList() throws IllegalFactoryArgument {
        log.info("TEST: grid.getShipList():");

        BsGrid defaultGrid = grid;
        BsGrid customGrid1 = new BsGrid((byte) 8, new HashMap<>() {{
            put(IBsShip.ShipType.Destroyer, 20);
        }});
        BsGrid customGrid2 = new BsGrid((byte) 8, new HashMap<>() {{
            put(IBsShip.ShipType.Cruiser, 5);
            put(IBsShip.ShipType.Destroyer, 10);
        }});

        List<IBsShip> defaultList = defaultGrid.getShipList();
        List<IBsShip> customList1 = customGrid1.getShipList();
        List<IBsShip> customList2 = customGrid2.getShipList();

        assertNotNull(defaultList);
        assertNotNull(customList1);
        assertNotNull(customList2);

        assertFalse(defaultList.isEmpty());
        assertFalse(customList1.isEmpty());
        assertFalse(customList2.isEmpty());

        /*
        Default List is not checked, as duplicate code could lead to discrepancies.
        If the custom Lists are correct, it is assumed the default one will work as intended as well.
        If in doubt, place a breakpoint and verify integrity manually.
        */

        Map<IBsShip.ShipType, Integer> customAmnt1 = new HashMap<>();
        for (IBsShip s : customList1) {
            IBsShip.ShipType sT = s.getType();
            Integer sTAmnt = customAmnt1.get(sT);
            customAmnt1.put(sT, (sTAmnt == null) ? 1 : sTAmnt + 1);
        }

        Map<IBsShip.ShipType, Integer> customAmnt2 = new HashMap<>();
        for (IBsShip s : customList2) {
            IBsShip.ShipType sT = s.getType();
            Integer sTAmnt = customAmnt2.get(sT);
            customAmnt2.put(sT, (sTAmnt == null) ? 1 : sTAmnt + 1);
        }

        assertEquals(20, (long) customAmnt1.get(IBsShip.ShipType.Destroyer));
        assertEquals(1, customAmnt1.keySet().size());

        assertEquals(5, (long) customAmnt2.get(IBsShip.ShipType.Cruiser));
        assertEquals(10, (long) customAmnt2.get(IBsShip.ShipType.Destroyer));
        assertEquals(2, customAmnt2.keySet().size());

        log.info("DONE: grid.getShipList - OK");
    }

    @Test
    public void placeShip() throws IllegalFactoryArgument, InvalidPlacementException {
        log.info("TEST: grid.placeShip():");

        //For convenience, a new ship is created. Normally, one of the existing ships would be passed as argument.
        IBsShip ship = placeAShip();

        BsField field = grid.getFieldCopy(bsc00);
        assertTrue(field.isOccupied());
        assertEquals(ship, field.getShip());

        Map<IBsShip, BsCoordinate[]> locations = grid.getShipLocations();
        assertArrayEquals(new BsCoordinate[]{bsc00}, locations.get(ship));

        log.info("DONE: grid.placeShip - OK");
    }

    @Test
    public void removeShip() throws IllegalFactoryArgument, InvalidPlacementException {
        log.info("TEST: grid.removeShip():");

        //For convenience, a new ship is created. Normally, one of the existing ships would be passed as argument.
        IBsShip ship = placeAShip();
        //Ship should be placed now, as per the placeShip test

        grid.removeShip(bsc00);

        BsField field = grid.getFieldCopy(bsc00);
        assertFalse(field.isOccupied());
        assertNull(field.getShip());

        assertNull(grid.getShipLocations().get(ship));
    }

    @Test
    public void attack() throws InvalidAttackException, InvalidPlacementException, IllegalFactoryArgument {
        log.info("TEST: grid.attack():");

        placeAShip();

        assertTrue(grid.attack(bsc00));
        assertFalse(grid.attack(bsc11));

        boolean eThrown = false;
        try {
            grid.attack(bsc00);
        } catch (InvalidAttackException e) {
            eThrown = true;
        }
        assertTrue(eThrown);

        log.info("DONE: grid.attack - OK");
    }

    @Test
    public void isAttacked() throws InvalidPlacementException, IllegalFactoryArgument, InvalidAttackException {
        log.info("TEST: grid.isAttacked():");

        placeAShip();
        grid.attack(bsc00);

        assertTrue(grid.isAttacked(bsc00));
        assertFalse(grid.isAttacked(bsc11));

        log.info("DONE: grid.isAttacked - OK");
    }

    @Test
    public void isOccupied() throws InvalidPlacementException, IllegalFactoryArgument {
        log.info("TEST: grid.isOccupied():");

        placeAShip();

        assertTrue(grid.isOccupied(bsc00));
        assertFalse(grid.isOccupied(bsc11));

        log.info("DONE: grid.isOccupied - OK");
    }

    @Test
    public void getShipLocation() throws InvalidPlacementException, IllegalFactoryArgument {
        log.info("TEST: grid.getShipLocation():");

        IBsShip ship = placeAShip();

        BsCoordinate origin = new BsCoordinate((byte) 0, (byte) 0);
        BsCoordinate[] origins = new BsCoordinate[]{origin};

        BsCoordinate[] coords = grid.getShipLocation(ship);
        assertArrayEquals(origins, coords);

        assertNull(grid.getShipLocation(wharf.createShip(IBsShip.ShipType.Destroyer)));

        log.info("DONE: grid.getShipLocation - OK");
    }

    @Test
    public void getShip() throws InvalidPlacementException, IllegalFactoryArgument {
        log.info("TEST: grid.getShip():");

        IBsShip ship = placeAShip();

        assertEquals(ship, grid.getShip(bsc00));
        assertNull(grid.getShip(bsc11));

        log.info("DONE: grid.getShip - OK");
    }

    private IBsShip placeAShip() throws IllegalFactoryArgument, InvalidPlacementException {
        IBsShip ship = wharf.createShip(IBsShip.ShipType.Destroyer);
        grid.placeShip(ship, bsc00, true);
        return ship;
    }


    @Test
    public void getFieldCopy() {
    }
}