package de.hdm_stuttgart.mi.se2.core.players;

import de.hdm_stuttgart.mi.se2.core.grid.BsCoordinate;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Random;

public class BsComputerPlayer extends ABsPlayer {

    private static Logger log = LogManager.getLogger(BsComputerPlayer.class);

    private Random random = new Random();

    /**
     * Creates a Computer player
     *
     * @param name     Player name
     * @param gridSize Length of the grid
     */

    BsComputerPlayer(String name, byte gridSize) {
        super(name, gridSize);
    }

    @Override
    public void placeShip(IBsShip shipDummy, byte xInputShipPosition, byte yInputShipPosition, boolean inputVertical) {

        List<IBsShip> computerShipList = grid.getShipList();

        for (IBsShip ship : computerShipList) {
            boolean unsuccesful = true;
            do {
                try {
                    boolean vertical = random.nextBoolean();
                    byte xShipPosition = (byte) random.nextInt(grid.getSize());
                    byte yShipPosition = (byte) random.nextInt(grid.getSize());

                    unsuccesful = !(grid.placeShip(ship, new BsCoordinate(xShipPosition, yShipPosition), vertical));

                    if(unsuccesful){
                        log.debug("AI Player: "+super.getPlayerName()+" , failed to place ship: "+ship+ " xPosition: " + xShipPosition + " yPosition: " + yShipPosition + " Vertical: " + vertical);
                    }else{
                        log.debug("Ship placed for AI Player, Ship: " + ship + " xPosition: " + xShipPosition + " yPosition: " + yShipPosition + " Vertical: " + vertical);

                    }

                } catch (InvalidPlacementException e) {
                    unsuccesful = true;
                }
            } while (unsuccesful);
        }
    }

    /**
     * Generate random coordinates and return a coordinate. Input parameters will be ignored.
     *
     * @param xInputShotPosition will be ignored
     * @param yInputShotPosition will be ignored
     * @return BsCoordinate with random x and y values
     */

    public BsCoordinate getShotCoordinate(byte xInputShotPosition, byte yInputShotPosition) {

        // Shot placement coordinates
        byte xShotPosition;
        byte yShotPosition;

        // Produce random coordinates to fire shot
        xShotPosition = (byte) random.nextInt(grid.getSize());
        yShotPosition = (byte) random.nextInt(grid.getSize());

        return new BsCoordinate(xShotPosition, yShotPosition);
    }
}
