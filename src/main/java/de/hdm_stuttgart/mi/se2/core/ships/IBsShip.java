package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import de.hdm_stuttgart.mi.se2.exceptions.ShipDeadException;

public interface IBsShip {

    byte getHealth();

    void hit() throws ShipDeadException;

    boolean isDead();

    void remove();

    byte getLength();

    boolean isPlaced();

    void place() throws InvalidPlacementException;

    enum ShipType {
        Carrier, Battleship, Cruiser, Destroyer
        //DO NOT EDIT THIS without adding a ABsShip subclass and a factory case for it.
    }

    ShipType getType();
}
