package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BsShipFactory {
    private static Logger log = LogManager.getLogger(BsShipFactory.class);

    public BsShipFactory() {
        log.debug("Creating ShipFactory");
    }

    public IBsShip createShip(IBsShip.ShipType type) throws IllegalFactoryArgument {
        switch (type) {
            case Carrier:
                log.trace("Creating Carrier");
                return new Carrier();
            case Battleship:
                log.trace("Creating Battleship");
                return new Battleship();
            case Cruiser:
                log.trace("Creating Cruiser");
                return new Cruiser();
            case Destroyer:
                log.trace("Creating Destroyer");
                return new Destroyer();
            default:
                log.error("A ship type has not yet been implemented in the Factory!");
                throw new IllegalFactoryArgument("Invalid ship type!");
        }
    }
}
