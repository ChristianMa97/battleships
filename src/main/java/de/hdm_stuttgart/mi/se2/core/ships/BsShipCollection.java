package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BsShipCollection {

    private static Logger log = LogManager.getLogger(BsShipCollection.class);

    private List<IBsShip> shipList; //List of all ships that exist for this collection
    private BsShipFactory wharf;

    public BsShipCollection(Map<IBsShip.ShipType, Integer> shipAmount) throws IllegalFactoryArgument {
        log.traceEntry();
        log.debug("Creating ShipCollection.");

        log.trace("Creating List and Factory");
        this.shipList = new ArrayList<>();
        this.wharf = new BsShipFactory();
        log.trace("List and factory initialized.");

        if (shipAmount == null) {
            log.trace("Collection initialized as empty.");
            return;
        }

        log.trace("Filling collection with ships");
        for (IBsShip.ShipType type : shipAmount.keySet()) {
            int amt = shipAmount.get(type);
            log.trace("Attempting to create " + amt + " ships of type " + type.toString());
            for (int i = 0; i < amt; i++) {
                this.createShip(type);
            }
        }

        log.trace("Collection initialized successfully");
        log.traceExit();
    }

    /**
     * Add ship to collection.
     *
     * @param ship The ship to be added
     */
    public void addShip(IBsShip ship) {
        log.trace("Adding ship of type " + ship.getType().toString() + " to ShipList...");
        this.shipList.add(ship);
    }

    public List<IBsShip> getShipList() {
        return new ArrayList<>(shipList);
    }

    public boolean contains(IBsShip ship) {
        return this.shipList.contains(ship);
    }

    public IBsShip createShip(IBsShip.ShipType type) throws IllegalFactoryArgument {
        log.traceEntry();
        log.trace("Creating ship of type " + type.toString() + "...");
        IBsShip ship = this.wharf.createShip(type);
        log.trace("Adding ship to collection...");
        this.addShip(ship);
        log.trace("Ship of type " + type.toString() + " created and added to List.");
        log.traceExit();
        return ship;
    }
}
