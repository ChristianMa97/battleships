package de.hdm_stuttgart.mi.se2.core.ships;

import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import de.hdm_stuttgart.mi.se2.exceptions.ShipDeadException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Abstract base class for the game's Ships.
 * Membership, position and orientation are set in this constructor
 * Length and accordingly starting health are set by the subclasses' constructor
 */
abstract class ABsShip implements IBsShip {


    private static Logger log = LogManager.getLogger(ABsShip.class);

    private final byte length;
    private final ShipType type;
    private byte health;
    private boolean placed;


    ABsShip(ShipType type, byte length) {
        log.trace("Ship of type " + type.toString() + " is being constructed");

        this.length = length;
        this.type = type;

        this.health = this.length;
        this.placed = false;

        log.trace("Ship of type " + type.toString() + " has been constructed");
    }

    /**
     * Lowers ship health by one, if possible.
     * Throws an exception if the ship is already dead, since it shouldn't be possible to fire at dead ships.
     * @throws ShipDeadException if the ship is already dead
     */
    @Override
    public void hit() throws ShipDeadException {

        log.trace("Ship is being hit.");
        if (this.isDead()) {
            throw new ShipDeadException();
        } else {
            this.health --;
            log.trace("New Health: " + this.health);
        }
    }

    /**
     * Determines and returns whether the ship is at zero health
     *
     * @return Returns true if the ship is dead
     */
    @Override
    public boolean isDead() {
        return 0 == health;
    }

    /**
     * For debugging purposes only: Returns current health.
     *
     * @return health
     */
    public byte getHealth() {
        return health;
    }

    public byte getLength() {
        return length;
    }

    @Override
    public boolean isPlaced() {
        return placed;
    }

    @Override
    public void place() throws InvalidPlacementException {
        if (!placed) {
            log.trace("Setting placed to true");
            this.placed = true;
        } else {
            throw new InvalidPlacementException("Ship was already placed!");
        }
    }

    @Override
    public void remove() {
        log.trace("Setting placed to false");
        this.placed = false;
    }


    @Override
    public ShipType getType() {
        return type;
    }
}
