package de.hdm_stuttgart.mi.se2.core.grid;

import de.hdm_stuttgart.mi.se2.core.ships.BsShipCollection;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidAttackException;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Field that is defined by its size. Each player places his ship on one grid.
 */
public class BsGrid {

    //Default list of ships to be created if no custom list is given
    private static final Map<IBsShip.ShipType, Integer> defaultShips = new HashMap<>() {{
        put(IBsShip.ShipType.Carrier, 1);
        put(IBsShip.ShipType.Battleship, 2);
        put(IBsShip.ShipType.Cruiser, 2);
        put(IBsShip.ShipType.Destroyer, 3);
    }};

    private static Logger log = LogManager.getLogger(BsGrid.class);

    private byte size;
    private BsField[][] fields;
    private BsShipCollection shipCollection;
    private Map<IBsShip, BsCoordinate[]> shipLocations;

    /**
     *Creates a grid with a default initial list of ships.
     *
     * @param gridSize Length of one side of the square grid
     */
    public BsGrid(byte gridSize) {
        log.debug("Creating grid with default ships");
        this.initDefault(gridSize);
    }

    /**
     * Creates a grid with a custom initial list of ships.
     *
     * @param gridSize   Length of one side of the square grid
     * @param shipAmount Map (ShipType to Integer) with the desired ShipTypes and the number of ships for each.
     * @throws IllegalFactoryArgument if the Map contains ShipTypes not yet implemented in the Factory.
     */
    public BsGrid(byte gridSize, Map<IBsShip.ShipType, Integer> shipAmount) throws IllegalFactoryArgument {
        log.debug("Creating grid with custom ships");
        init(gridSize, shipAmount);
    }

    private void initDefault(byte gridSize) {
        try {
            init(gridSize, defaultShips);
        } catch (IllegalFactoryArgument illegalFactoryArgument) {
            log.error("DefaultShips contains illegal values. Collection may not have been completed!");
        }
    }

    private void init(byte gridSize, Map<IBsShip.ShipType, Integer> shipAmount) throws IllegalFactoryArgument {
        log.traceEntry();

        log.trace("Creating fields matrix");
        this.size = gridSize;
        this.fields = new BsField[gridSize][gridSize];


        log.trace("Filling matrix with fields");
        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < fields[i].length; j++) {
                fields[i][j] = new BsField();
            }
        }

        log.trace("Creating ShipCollection and ShipLocations");
        this.shipCollection = new BsShipCollection(shipAmount);
        this.shipLocations = new HashMap<>();

        log.traceExit();
    }

    /**
     * Getter for the fields for internal use
     *
     * @param position the BsCoordinate at which the desired field should be.
     * @return BsField at the given coordinate
     */
    private BsField getField(BsCoordinate position) {
        return fields[position.x][position.y];
    }

    public BsField getFieldCopy(BsCoordinate coordinate) {
        return new BsField(this.getField(coordinate));
    }

    private BsCoordinate[] getShipRectangle(BsCoordinate topleft, byte length, boolean vertical) {
        log.traceEntry();
        byte xLength, yLength;

        if (vertical) {
            log.trace("Setting vertical length");
            xLength = 1;
            yLength = length;
        } else {
            log.trace("Setting horizontal length");
            xLength = length;
            yLength = 1;
        }

        log.traceExit();
        return rectangle(topleft, xLength, yLength);
    }

    /**
     * Checks if the placement of a ship with the given parameters is possible.
     *
     * @param topleft Coordinate at the top/left end of the placement
     * @param ship Ship to check placement for
     * @param vertical whether the ship is to be placed vertically
     * @return true if placement is possible, false if not
     * @throws IllegalArgumentException if the placement is already outside of bounds
     */
    public boolean checkPlacement(BsCoordinate topleft, IBsShip ship, boolean vertical) {
        return checkPlacement(topleft, ship.getLength(), vertical);
    }

    /**
     * Checks if the placement of a ship with the given parameters is possible.
     *
     * @param topleft Coordinate at the top/left end of the placement
     * @param length length of the ship
     * @param vertical whether the ship is to be placed vertically
     * @return true if placement is possible, false if not
     * @throws IllegalArgumentException if the placement is already outside of bounds
     */
    public boolean checkPlacement(BsCoordinate topleft, byte length, boolean vertical) {
        log.traceEntry();
        log.trace("Checking if the arguments are valid");
        if (topleft.x < 0 || topleft.y < 0) throw new IllegalArgumentException("Invalid placement values: below zero");
        if (topleft.x >= size || topleft.y >= size)
            throw new IllegalArgumentException("Invalid placement values: above size");
        if (length < 1) throw new IllegalArgumentException("Invalid placement values: length below one");

        log.trace("Checking if there is enough space");
        //If vertical, see if there is enough space from the top left position down
        //If horizontal, see if there is enough space from the top left position to the right
        if (((vertical) ? topleft.y : topleft.x) + length > size) {
            log.traceExit();
            return false;
        }

        log.trace("Finding surrounding BsCoordinates");
        BsCoordinate[] spaces;
        if (vertical) {
            spaces = findSurroundingSpaces(topleft, (byte) 1, length);
        } else {
            spaces = findSurroundingSpaces(topleft, length, (byte) 1);
        }

        for (BsCoordinate c : spaces) {
            if (isOccupied(c)) {
                log.traceExit();
                return false;
            }
        }

        log.traceExit();
        return true;
    }

    public IBsShip getShip(BsCoordinate coordinate) {
        return getField(coordinate).getShip();
    }

    public BsCoordinate[] getShipLocation(IBsShip ship) {
        return shipLocations.get(ship);
    }

    public Map<IBsShip, BsCoordinate[]> getShipLocations() {
        return new HashMap<>(shipLocations);
    }

    public boolean isOccupied(BsCoordinate coordinate) {
        return getField(coordinate).isOccupied();
    }

    public boolean isAttacked(BsCoordinate coordinate) {
        return getField(coordinate).isAttacked();
    }

    public boolean attack(BsCoordinate coordinate) throws InvalidAttackException {
        return getField(coordinate).attack();
    }

    public byte getSize() {
        return this.size;
    }

    public List<IBsShip> getShipList() {
        return new ArrayList<>(shipCollection.getShipList());
    }

    /**
     * Place given ship on the grid with the top left corner at the given position and the given orientation.
     *
     * @param ship Ship to be placed
     * @param topLeft Top left coordinate of the target placement
     * @param vertical Verticality of the placement
     * @return true if placement was successful, false if not.
     * @throws InvalidPlacementException If a coordinate is unexpectedly occupied despite the placement check
     */
    public boolean placeShip(IBsShip ship, BsCoordinate topLeft, boolean vertical) throws InvalidPlacementException {
        log.traceEntry();

        if (checkPlacement(topLeft, ship.getLength(), vertical)) {
            log.trace("Placing ship of type " + ship.getType() + " at " + topLeft);
            BsCoordinate[] coordinates = getShipRectangle(topLeft, ship.getLength(), vertical);
            for (BsCoordinate c : coordinates) {
                getField(c).placeShip(ship);
            }
            ship.place();
            shipLocations.put(ship, coordinates);

            log.traceExit();
            return true;
        } else {
            log.traceExit();
            return false;
        }
    }

    public void removeShip(BsCoordinate coordinate) {
        log.traceEntry();

        IBsShip ship = getShip(coordinate);

        BsCoordinate[] coords = getShipLocation(ship);
        for (BsCoordinate c : coords) {
            getField(c).removeShip(ship);
        }
        shipLocations.remove(ship);

        log.traceExit();
    }

    private BsCoordinate[] findSurroundingSpaces(BsCoordinate topleft, byte xLength, byte yLength) {
        BsCoordinate outerTL = new BsCoordinate((byte) (topleft.x - 1), (byte) (topleft.y - 1));
        //If this is outside the actual grid, it won't matter:
        // - the actual ship's spaces are verified in checkPlacement
        // - rectangle will automatically trim to the grid's borders
        //Thus, no invalid coordinates will be returned.
        return rectangle(outerTL, xLength + 2, yLength+2);
    }

    /**
     * Find all the [x, y] pairs within the grid in a given rectangle, checking for grid integrity.
     *
     * If the boundaries extend in any direction past the Grid's limits, the rectangle will automatically be trimmed.
     *
     * @param topleft the top left corner of the rectangle
     * @param xLength length of the rectangle in x-direction
     * @param yLength length of the rectangle in y-direction
     * @return BsCoordinate array comprised of all the BsCoordinates within the given dimensions
     */
    private BsCoordinate[] rectangle(BsCoordinate topleft, int xLength, int yLength) {
        log.traceEntry();

        log.trace("Determining coordinates describing a rectangle starting from " + topleft.toString() + " and extending "
                + xLength + " fields in the direction of x and " + yLength + " in that of y.");

        //Approach: Take the greater of the Top Left and 0 as start. This ensures that the lowest possible index is 0
        //Similarly, take the lesser of the Bottom Right and the size as limit. Thus, the index will stay within bounds

        byte xmin = (byte) Math.max(0, topleft.x);
        byte xmax = (byte) Math.min(size, topleft.x + xLength);
        byte ymin = (byte) Math.max(0, topleft.y);
        byte ymax = (byte) Math.min(size, topleft.y + yLength);

        //effective length x * effective length y
        BsCoordinate[] rect = new BsCoordinate[(xmax - xmin)* (ymax - ymin)];

        int index = 0;

        for (byte x = xmin; x < xmax; x++) {
            for (byte y = ymin; y < ymax; y++) {
                rect[index] = new BsCoordinate(x, y);
                index ++;
            }
        }

        log.traceExit();
        return rect;
    }
}
