package de.hdm_stuttgart.mi.se2.core.ships;

class Cruiser extends ABsShip {
    Cruiser() {
        super(ShipType.Cruiser, (byte) 2);
    }
}
