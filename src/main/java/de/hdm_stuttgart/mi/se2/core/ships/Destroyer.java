package de.hdm_stuttgart.mi.se2.core.ships;

class Destroyer extends ABsShip {
    Destroyer() {
        super(ShipType.Destroyer, (byte) 1);
    }
}
