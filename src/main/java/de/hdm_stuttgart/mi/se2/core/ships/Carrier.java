package de.hdm_stuttgart.mi.se2.core.ships;

class Carrier extends ABsShip {
    Carrier() {
        super(ShipType.Carrier, (byte) 4);
    }
}