package de.hdm_stuttgart.mi.se2.core.players;

import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class BsPlayerFactory {

    private static Logger log = LogManager.getLogger(BsPlayerFactory.class);

    /**
     * @param humanNumber Number of human players
     * @param aiNumber Number of computer
     */

    private byte humanNumber = 0;
    private byte aiNumber = 0;

    /**
     *
     * Creates computer and human players
     *
     * @param player Either of type human or computer
     * @param gridSize Length of the grid
     * @return Either computer, human player or throws exception
     * @throws IllegalFactoryArgument If Playertype is wrong exception is thrown
     */

    public IBsPlayer createPlayer(IBsPlayer.Playertype player, byte gridSize) throws IllegalFactoryArgument {


        switch (player) {
            case HUMANPLAYER:
                humanNumber++;
                String name = "Human" + humanNumber;
                return new BsHumanPlayer(name, gridSize);

            case AIPLAYER:
                aiNumber++;
                String nameAI = "AI" + aiNumber;
                return new BsComputerPlayer(nameAI, gridSize);

            default:
                log.error("wrong playerType" + player);
                throw new IllegalFactoryArgument("Wrong playerType!");
        }
    }
}