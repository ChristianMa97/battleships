package de.hdm_stuttgart.mi.se2.core.players;

import de.hdm_stuttgart.mi.se2.core.grid.BsCoordinate;
import de.hdm_stuttgart.mi.se2.core.grid.BsGrid;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;

public interface IBsPlayer {

    enum Playertype {
        HUMANPLAYER,
        AIPLAYER;

        Playertype() {
        }
    }

    String getPlayerName();

    void changePlayerName(String playerName);

    BsGrid getGrid();

    void placeShip(IBsShip ship, byte xInputShipPosition, byte yInputShipPosition, boolean inputVertical) throws InvalidPlacementException;

    BsCoordinate getShotCoordinate(byte xInputCoordinate, byte yInputCoordinate);

    byte getRemainingHealthPoints();
}