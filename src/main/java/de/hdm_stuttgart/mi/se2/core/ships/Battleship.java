package de.hdm_stuttgart.mi.se2.core.ships;

class Battleship extends ABsShip {
    Battleship() {
        super(ShipType.Battleship, (byte) 3);
    }
}
