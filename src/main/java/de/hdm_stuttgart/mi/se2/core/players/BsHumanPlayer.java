package de.hdm_stuttgart.mi.se2.core.players;

import de.hdm_stuttgart.mi.se2.core.grid.BsCoordinate;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BsHumanPlayer extends ABsPlayer {

    private static Logger log = LogManager.getLogger(BsHumanPlayer.class);

    /**
     * Creates a human player
     *
     * @param name     Player name
     * @param gridSize Length of the grid
     */

    BsHumanPlayer(String name, byte gridSize) {
        super(name, gridSize);
    }

    @Override
    public void placeShip(IBsShip ship, byte xInputShipPosition, byte yInputShipPosition, boolean inputVertical) throws InvalidPlacementException {
        try {
            grid.placeShip(ship, new BsCoordinate(xInputShipPosition, yInputShipPosition), inputVertical);
            log.debug("Ship placed for " + super.getPlayerName() + ", Ship: " + ship + " xPosition " + xInputShipPosition + " xPosition " + yInputShipPosition + " Vertical: " + inputVertical);
        } catch (InvalidPlacementException i) {
            log.error("FATAL");
            throw i;
        }
    }

    /**
     * Generate coordinate for for shot according to input values x and y from GUI.
     *
     * @param xInputShotPosition wanted xPosition
     * @param yInputShotPosition wanted yPosition
     * @return BsCoordinate
     */

    @Override
    public BsCoordinate getShotCoordinate(byte xInputShotPosition, byte yInputShotPosition) {
        return new BsCoordinate(xInputShotPosition, yInputShotPosition);
    }
}
