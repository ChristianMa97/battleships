package de.hdm_stuttgart.mi.se2.core.grid;

public class BsCoordinate {

    /**
     * Coordinate fields
     * <p>
     * public for convenient access, final to prevent unwanted alterations.
     */
    public final byte x, y;

    public BsCoordinate(byte x, byte y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        return this.x;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        } else {
            BsCoordinate bsc = (BsCoordinate) obj;
            return (bsc.x == this.x && bsc.y == this.y);
        }
    }

    @Override
    public String toString() {
        return "(" + x + "|" + y + ")";
    }

    public byte[] toArray() {
        return new byte[]{x, y};
    }

}
