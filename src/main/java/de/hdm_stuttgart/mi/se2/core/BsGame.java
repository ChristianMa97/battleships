package de.hdm_stuttgart.mi.se2.core;

import de.hdm_stuttgart.mi.se2.core.grid.BsCoordinate;
import de.hdm_stuttgart.mi.se2.core.players.BsComputerPlaceShipsThread;
import de.hdm_stuttgart.mi.se2.core.players.BsComputerPlayer;
import de.hdm_stuttgart.mi.se2.core.players.BsPlayerFactory;
import de.hdm_stuttgart.mi.se2.core.players.IBsPlayer;
import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidAttackException;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidNumberOfAiPlayer_Exception;
import de.hdm_stuttgart.mi.se2.javaFX.MainMenu;
import de.hdm_stuttgart.mi.se2.javaFX.PlaceShipsController;
import de.hdm_stuttgart.mi.se2.javaFX.TurnController;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;


public class BsGame {
    private static Logger log = LogManager.getLogger(BsGame.class);


    private byte playerNumber; //Number of Players, AI and Human in sum.
    private byte amountOfAiPlayer; //Amount of AI Player, at least one HumanPlayer need to remain.
    private IBsPlayer[] playerList;   //Array's length equals the playerNumber and contains the reference of each player
    private byte gridSize;
    private BsPlayerFactory factory = new BsPlayerFactory();
    private TextField[] names; //Contains the playernames for each player and computerPlayer
    private int counterForCreatingViewPlaceShips = 0; //needed to call the view place ships for each human player
    private IBsPlayer currentTurn;  //Contains the reference to the player with can make a move
    private List<BsCoordinate> attackedFromComputer = new LinkedList<>();  //List with all Coordinates that have already been attacked from AI


    /**
     * Default Constructor with the default Values:
     * playerNumber=1
     * gridSize=9
     * 1 AI-Player
     * If the Values are not changed through configureGame the Game will start with with these pre-configured Values
     */
    public BsGame(){
        log.debug("BsGame was initiated with default Constructor");

        playerNumber=2;
        gridSize=9;
        amountOfAiPlayer=1;
    }


    /**
     * Starts the Game and initiate the Player.
     * @throws IllegalFactoryArgument if an wrong argument passed to player factory
     */
    public void startGame() throws IllegalFactoryArgument {
        log.debug("Start Game has been called");
        playerList= new IBsPlayer[playerNumber];        //Initiate the Array with the size of playerNumber
        int counter=0;
        log.trace("Array of Players has been created with size:"+playerNumber);

        try {
            //creates the Human Player
            for(int i=0; i<playerNumber-amountOfAiPlayer;i++){
                playerList[i] = factory.createPlayer(IBsPlayer.Playertype.HUMANPLAYER, gridSize);
                if(names!=null) {
                    playerList[i].changePlayerName(names[counter].getText());
                    counter++;
                }
                log.trace("Human-Player created on Index:"+i);
            }
            //Creates the AI Player
            for (int i =playerNumber-amountOfAiPlayer; i < playerNumber; i++) {
                playerList[i] = factory.createPlayer(IBsPlayer.Playertype.AIPLAYER, gridSize);
                log.trace("AI-Player created on Index:"+i);
            }


        }catch(IllegalFactoryArgument e){//Wrong Argument was passed to Factory, should not happen
            log.error("IllegalFactoryArgument Exception occured in BsGame StartGame");
            throw e;
        }
        //Set the ships for the AI-Player on Grid.
        for(int i=playerNumber-amountOfAiPlayer; i< playerNumber;i++){

            if(playerList[i] instanceof BsComputerPlayer) {
                log.debug("placing ships for ComputerPlayer: "+ playerList[i].getPlayerName());
                BsComputerPlayer computerPlayer = (BsComputerPlayer) playerList[i];
                //Start new Thread
                new BsComputerPlaceShipsThread(computerPlayer).start();
                //No reference needed - we never make sure he is quite done,
                //We just assume no human player can place ships faster than the computer(s)
            }
        }
        openViewPlaceShips(playerList[counterForCreatingViewPlaceShips]);
    }
    private void goAhead(){
        new TurnController(MainMenu.getPrimaryStage(),this, playerList[0], playerList[1]);
    }


    public void changePlayerTurn(){
        if(currentTurn==null){
            currentTurn=playerList[0];
            log.debug("Turn changed to Player1");
        }
        if(currentTurn.equals(playerList[0])){
            currentTurn=playerList[1];
            log.debug("Turn changed to Player2/ComputerPlayer");

        }else{
            currentTurn=playerList[0];
            log.debug("Turn changed to Player1");

        }
    }

    public IBsPlayer getCurrentTurn(){
    if(currentTurn==null){
        currentTurn=playerList[0];
    }
        return currentTurn; //Need to return reference
    }

    private void openViewPlaceShips(IBsPlayer player){
        new PlaceShipsController(MainMenu.getPrimaryStage(), this, player);
    }

    public void submitButtonKlicked(){
        counterForCreatingViewPlaceShips++;
        if(counterForCreatingViewPlaceShips <playerNumber-amountOfAiPlayer){
            openViewPlaceShips(playerList[counterForCreatingViewPlaceShips]);
        }else{
            goAhead();
        }
    }


    /**
     * @param playerNumber Sets the playerNumber in sum AI-Player and Human-Player.
     * @param gridSize      The wanted gridSize.
     * @param amountOfAiPlayer  The amount of AI-Player in the Game
     * @param names Configured name fields with contain the player names
     * @throws InvalidNumberOfAiPlayer_Exception If no HumanPlayer remains the Exception is thrown.
     */
    public void configureGame(byte playerNumber, byte gridSize, byte amountOfAiPlayer, TextField[] names) throws InvalidNumberOfAiPlayer_Exception {
        log.debug("configure Game was called with playerNumber:"+playerNumber+" gridSize:"+gridSize+" amountOfAiPlayer:"+ amountOfAiPlayer);

        if(amountOfAiPlayer<playerNumber) {
            this.names=names;
            this.gridSize = gridSize;
            this.playerNumber = playerNumber;
            this.amountOfAiPlayer = amountOfAiPlayer;
        }else{
            log.debug("playerNumber is not larger than amountOfAIPlayer. playerNumber:"+playerNumber+" amountOfAIPlayer:"+amountOfAiPlayer);
            throw new InvalidNumberOfAiPlayer_Exception("At least one Human Player need to be assigned.");
        }

    }

    /**
     * First calls the the Player if the player is a Human-Player he return the associated BsCoordinate
     * if the player is a Computer he returns a random BsCoordinate.
     * Then the method attacks the respectively associated opponent.
     *
     * @param xPosition xPositon to attack
     * @param yPosition yPosition to atack
     * @return returns true if the shot has hit a ship
     */
    public boolean placeShot(byte xPosition, byte yPosition){
        try {
            boolean hasHit;

                IBsPlayer oppositePlayer;
                if (getCurrentTurn().equals(playerList[0])) {
                    oppositePlayer=playerList[1];
                } else {
                    oppositePlayer=playerList[0];
                }

            if (currentTurn instanceof BsComputerPlayer) {
                log.trace(currentTurn.getPlayerName()+" Turn");
                BsCoordinate bsCoordinate;
                do {
                    do{
                        //Get Random BsCoordinate from ComputerPlayer and check if its already attacked
                        bsCoordinate=currentTurn.getShotCoordinate(xPosition, yPosition);
                    }while (oppositePlayer.getGrid().isAttacked(bsCoordinate));
                    //attack opposite Player
                    log.trace("Computer selected x: "+ bsCoordinate.x+" y: "+bsCoordinate.y);
                    hasHit = oppositePlayer.getGrid().attack(bsCoordinate);
                    attackedFromComputer.add(bsCoordinate);//adding coordinate to the attacked ones
                } while (hasHit);
                return false;
            } else { //HumanPlayer
                return oppositePlayer.getGrid().attack(currentTurn.getShotCoordinate(xPosition, yPosition));
            }
        }catch (InvalidAttackException e){
            //Should not appear is already prevented in GUI
            log.fatal("Invalid Attack Exception thrown in BsGame.public placeShot "+e);
            MainMenu.terminateProgramWithMessage(4);
            return false;
        }
    }


    public List<BsCoordinate> getCopyOfAttackedFieldsFromComputer(){
        return List.copyOf(attackedFromComputer);

    }

}
