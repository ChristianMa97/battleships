package de.hdm_stuttgart.mi.se2.core.players;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BsComputerPlaceShipsThread extends Thread {
    private BsComputerPlayer player;

    private static Logger  log = LogManager.getLogger(BsComputerPlaceShipsThread.class);

    public BsComputerPlaceShipsThread(BsComputerPlayer player) {
        super();
        this.player = player;
    }

    @Override
    public void run() {
        log.info("Thread BsComputerPlayer place ships started");
        player.placeShip(null, (byte) 0, (byte) 0, true);
        log.info("Thread BsComputerPlayer place ships finished");
    }
}
