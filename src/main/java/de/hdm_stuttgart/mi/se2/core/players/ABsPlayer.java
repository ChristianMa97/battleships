package de.hdm_stuttgart.mi.se2.core.players;

import de.hdm_stuttgart.mi.se2.core.grid.BsGrid;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;


public abstract class ABsPlayer implements IBsPlayer {

    private static Logger log = LogManager.getLogger(ABsPlayer.class);

    private String playername;
    protected BsGrid grid;


    ABsPlayer(String name, byte gridSize) {
        this.playername = name;
        grid = new BsGrid(gridSize);
        log.debug("Player created with Name: " + playername + " Grid: " + grid);
    }

    @Override
    public String getPlayerName() {
        return playername;
    }

    @Override
    public void changePlayerName(String playerName) {
        this.playername = playerName;
        log.debug("Player name has been changed to:" + playerName);
    }

    @Override
    public BsGrid getGrid() {
        return grid;
    }


    /**
     * Get the health points of all ships and return the sum
     *
     * @return Remaining health (total of hits player can still take)
     */

    @Override
    public byte getRemainingHealthPoints() {

        byte remainingHealthPoints = 0;

        List<IBsShip> shipsListRemaining =
                grid.getShipList()
                        .stream()
                        .filter(ship -> !(ship.isDead()))
                        .collect(Collectors.toList());

        for (int i = 0; i < shipsListRemaining.size(); i++) {
            remainingHealthPoints += shipsListRemaining.get(i).getHealth();
        }

        return remainingHealthPoints;
    }
}
