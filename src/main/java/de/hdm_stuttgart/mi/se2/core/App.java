package de.hdm_stuttgart.mi.se2.core;

import de.hdm_stuttgart.mi.se2.core.players.BsComputerPlayer;
import de.hdm_stuttgart.mi.se2.javaFX.MainMenu;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static javafx.application.Application.launch;

public class App {

    private static Logger log = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        log.info("Program started");
        Application.launch(MainMenu.class, args);
    }
}
