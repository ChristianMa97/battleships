package de.hdm_stuttgart.mi.se2.core.grid;

import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidAttackException;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidPlacementException;
import de.hdm_stuttgart.mi.se2.exceptions.ShipDeadException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BsField {

    private static Logger log = LogManager.getLogger(BsField.class);

    private boolean attacked;

    private IBsShip ship;

    BsField() {
        this(null, false);
    }

    BsField(IBsShip ship, boolean attacked) {
        log.trace("Creating new Field");
        this.ship = ship;
        this.attacked = attacked;
    }

    BsField(BsField field) {
        log.trace("Creating copy of Field");
        this.ship = field.ship;
        this.attacked = field.attacked;
    }

    /**
     * Attempts an attack on the coordinate, performing a hit on a ship, if there is one.
     *
     * @return If a ship was hit, returns true.
     * @throws InvalidAttackException if the ship is already dead or the field has been attacked before
     */
    boolean attack() throws InvalidAttackException {
        log.traceEntry();
        if(!this.attacked) {
            this.attacked = true;
            try {
                if (ship == null) {
                    log.debug("Coordinate attacked, but no ship was hit. Returning false.");
                    log.traceExit();
                    return false; //nothing to hit, no ship is destroyed, return false
                } else {
                    log.debug("Coordinate attacked, ship is being hit. Returning true.");
                    this.ship.hit();
                    log.traceExit();
                    return true; //ship was hit, return true
                }
            } catch (ShipDeadException e) {
                log.debug("Ship was already dead, throwing exception");
                throw new InvalidAttackException(e.getMessage());
            }
        } else {
            log.debug("Coordinate had been attacked before, throwing exception");
            throw new InvalidAttackException("Area has already been attacked before.");
        }
    }


    void placeShip(IBsShip ship) throws InvalidPlacementException {
        if (!isOccupied()) {
            this.ship = ship;
            log.trace("Ship placed on coordinate");
        } else {
            throw new InvalidPlacementException("Area already occupied!");
        }

    }

    boolean isAttacked() {
        return attacked;
    }

    boolean isOccupied() {
        return !(null == this.ship);
    }

    /**
     * Removes the ship from this coordinate iff it is the ship in question
     *
     * @param ship ship to compare this coordinate's ship against, and remove, if matching
     */
    void removeShip(IBsShip ship) {
        if (this.ship == ship) removeShip();
    }

    /**
     * Removes ship from this coordinate indiscriminately
     */
    void removeShip() {
        this.ship = null;
    }

    IBsShip getShip() {
        return ship;
    }
}
