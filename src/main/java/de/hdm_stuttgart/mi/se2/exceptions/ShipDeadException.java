package de.hdm_stuttgart.mi.se2.exceptions;

public class ShipDeadException extends Exception {
    public ShipDeadException() {
        this("Ship is already dead.");
    }

    public ShipDeadException(String message) {
        super(message);
    }
}
