package de.hdm_stuttgart.mi.se2.exceptions;

public class IllegalFactoryArgument extends Exception {
    public IllegalFactoryArgument( String message ) {
        super(message);
    }

}
