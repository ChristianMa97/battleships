package de.hdm_stuttgart.mi.se2.exceptions;

public class InvalidPlacementException extends Exception {
    public InvalidPlacementException(String message) {
        super(message);

        /*Expected messages:
          Area is already occupied.
          Ship is placed outside bounds.
          */
    }
}
