package de.hdm_stuttgart.mi.se2.exceptions;

public class InvalidAttackException extends Exception {
    public InvalidAttackException() {
        this("Coordinate has already been attacked.");
    }

    public InvalidAttackException(String message) {
        super("Invalid Attack. " + message);
    }
}
