package de.hdm_stuttgart.mi.se2.exceptions;

public class InvalidNumberOfAiPlayer_Exception extends Exception {
    public InvalidNumberOfAiPlayer_Exception(String message) {
        super(message);
    }
}
