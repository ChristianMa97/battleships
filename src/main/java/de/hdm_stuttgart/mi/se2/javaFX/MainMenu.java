package de.hdm_stuttgart.mi.se2.javaFX;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

public class MainMenu extends Application {
    private static Logger log = LogManager.getLogger(MainMenu.class);
    private final static Stage primaryStage = new Stage();  //Main Stage for the Game

    @Override
    public void start(Stage notUsedStage) {
        notUsedStage.close();
        log.trace("MainMenu View created");

     try{
         FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainMenu.fxml"));
         AnchorPane pane = loader.load();
         primaryStage.setMinWidth(600.00);
         primaryStage.setMinHeight(600.00);
         Scene scene= new Scene(pane);
         log.trace("scene set");
         MainMenuController mainMenuController=loader.getController();
         log.trace("assigned mainMenuController with loader.getController()"+ mainMenuController);
         primaryStage.setTitle("Battleships MainMenu");
         primaryStage.setScene(scene);
         primaryStage.show();


     }catch (Exception e){
        log.fatal("Exception thrown in MainMenu Programm will be terminated"+e.getStackTrace());
        terminateProgramWithMessage(1);

     }

    }

    //Pop up new Stage with ErrorMessage and terminate the Program after a delay
    public static void terminateProgramWithMessage(final int errorCode){
        log.error("display terminateProgramWithMessage and terminate program after delay");
        Stage errorStage = new Stage();
        errorStage.setTitle("Error");
        errorStage.setMinHeight(300);
        errorStage.setMinWidth(500);
        AnchorPane errorPane = new AnchorPane();
        Scene errorScene = new Scene(errorPane);
        Label errorLabel = new Label("Error "+errorCode+" occured, Game will be closed");
        errorLabel.setStyle("-fx-font-size: 1.5em");
        errorPane.getChildren().add(errorLabel);
        errorStage.setScene(errorScene);
        errorStage.show();

        //Terminating the program after a specific delay
        // creating timer task, timer
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.exit(0);
            }
        };
        Timer timer = new Timer();

        // scheduling the task at interval
        timer.schedule(timerTask, 10000);
    }

    public static Stage getPrimaryStage(){
        return primaryStage;
    }
}
