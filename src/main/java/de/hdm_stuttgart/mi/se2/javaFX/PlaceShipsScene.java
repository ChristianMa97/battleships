package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.players.IBsPlayer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class PlaceShipsScene extends Application {
    private static Logger log = LogManager.getLogger(PlaceShipsScene.class);


    private Stage primaryStage;
    private IBsPlayer player;
    private Button[][] buttonsOnField;
    private ToggleButton tbHorizontal;
    private ToggleButton tbVertical;
    private ToggleButton tbCarrier;
    private ToggleButton tbBattleship;
    private ToggleButton tbCruiser;
    private ToggleButton tbDestroyer;
    private TextField tfCarrier;
    private TextField tfBattleship;
    private TextField tfCruiser;
    private TextField tfDestroyer;
    private Label errorLabel;
    private Button submit;


    PlaceShipsScene(Stage primaryStage, IBsPlayer player){
        this.primaryStage=primaryStage;
        this.player=player;
        start(primaryStage);
    }

    @Override
    public void start(Stage stage){
            BorderPane pane = new BorderPane();
            Scene scene = new Scene(pane);
            scene.getStylesheets().add((getClass().getResource("/css/PlaceShipsScene.css")).toExternalForm()); //add Stylesheet
            HBox hBox = new HBox();//HBox for seperating buttons and field
            hBox.setSpacing(15);
            pane.setCenter(hBox);

            //creating Field out of buttons
            CreateFieldOutOfButtons createFieldOutofButtons = new CreateFieldOutOfButtons(player);
            GridPane gridPane= createFieldOutofButtons.getGridPane();
            buttonsOnField=createFieldOutofButtons.getButtonsOnField();
            hBox.getChildren().add(gridPane);
            VBox vBox = new VBox(); //VBox for Controls
            hBox.getChildren().add(vBox);

            //ToggleButton Vertical/Horizontal
            HBox tbHBox = new HBox();
            vBox.getChildren().add(tbHBox);
            ToggleGroup groupVerticalHorizontal = new ToggleGroup();
            tbHorizontal = new ToggleButton("Horizontal");
            tbVertical = new ToggleButton("Vertikal");
            tbHorizontal.setToggleGroup(groupVerticalHorizontal);
            tbVertical.setToggleGroup(groupVerticalHorizontal);
            tbHorizontal.setSelected(true);
            tbHBox.getChildren().add(tbHorizontal);
            tbHBox.getChildren().add(tbVertical);

            //ToggleButtons Shiptype
            ToggleGroup shipTypes = new ToggleGroup();
            tbCarrier = new ToggleButton("Carrier");
            tbBattleship = new ToggleButton("Battleship");
            tbCruiser = new ToggleButton("Cruiser");
            tbDestroyer = new ToggleButton("Destroyer");
            tbCarrier.getStyleClass().add("shipButtons");
            tbBattleship.getStyleClass().add("shipButtons");
            tbCruiser.getStyleClass().add("shipButtons");
            tbDestroyer.getStyleClass().add("shipButtons");
            tbCarrier.setToggleGroup(shipTypes);
            tbBattleship.setToggleGroup(shipTypes);
            tbCruiser.setToggleGroup(shipTypes);
            tbDestroyer.setToggleGroup(shipTypes);
            tbCarrier.setSelected(true);
            HBox hBoxCarrier = new HBox();
            HBox hBoxBattleship = new HBox();
            HBox hBoxCruiser = new HBox();
            HBox hBoxDestroyer = new HBox();
            vBox.getChildren().addAll(hBoxCarrier, hBoxBattleship, hBoxCruiser, hBoxDestroyer);


            //Create Textfields and add Button and Textfield to Layout
            tfCarrier = new TextField();
            tfCarrier.setEditable(false);
            tfBattleship = new TextField();
            tfBattleship.setEditable(false);
            tfCruiser = new TextField();
            tfCruiser.setEditable(false);
            tfDestroyer = new TextField();
            tfDestroyer.setEditable(false);
            hBoxCarrier.getChildren().addAll(tbCarrier, tfCarrier);
            hBoxBattleship.getChildren().addAll(tbBattleship, tfBattleship);
            hBoxCruiser.getChildren().addAll(tbCruiser, tfCruiser);
            hBoxDestroyer.getChildren().addAll(tbDestroyer, tfDestroyer);

            //Create Bottom Box with errorLabel and Submit Button
            HBox bottomHBox = new HBox();
            bottomHBox.setSpacing(30);
            errorLabel = new Label();
            errorLabel.setId("errorLabel");
            errorLabel.setAlignment(Pos.BASELINE_LEFT);
            bottomHBox.getChildren().add(errorLabel);
            submit = new Button("Submit");
            submit.setId("submit");
            bottomHBox.getChildren().add(submit);
            pane.setBottom(bottomHBox);

            primaryStage.setTitle("Place Ships of Player: "+ player.getPlayerName());
            primaryStage.setScene(scene);
            primaryStage.show();
    }
    //Methods to set EventHandler
     void setEventHandlerTbVertical(EventHandler e){
        tbVertical.setOnAction(e);
    }   void setEventHandlerTbHorizontal(EventHandler e){
        tbHorizontal.setOnAction(e);
    }   void setEventHandlerTbCarrier(EventHandler e){
        tbCarrier.setOnAction(e);
    }   void setEventHandlerTbBattleship(EventHandler e){
        tbBattleship.setOnAction(e);
    }   void setEventHandlerTbCruiser(EventHandler e){
        tbCruiser.setOnAction(e);
    }   void setEventHandlerTbDestroyer(EventHandler e){
        tbDestroyer.setOnAction(e);
    }  void setEventHandlerSubmit(EventHandler e){
        submit.setOnAction(e);
    }

    //Methods to change Textfields
     void setTfCruiser(String e){
        tfCruiser.setText(e);
    }
     void setTfCarrier(String e){
        tfCarrier.setText(e);
    }
     void setTfBattleship(String e){
        tfBattleship.setText(e);
    }
     void setTfDestroyer(String e){
        tfDestroyer.setText(e);
    }
     void setErrorLabel(String e){
        errorLabel.setText(e);
    }

    Button[][] getButtonsOnField(){
        return Arrays.copyOf(buttonsOnField, buttonsOnField.length);
    }

}
