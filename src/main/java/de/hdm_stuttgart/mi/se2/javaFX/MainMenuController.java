package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.BsGame;
import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import javafx.fxml.FXML;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainMenuController {
    private static Logger log = LogManager.getLogger(MainMenuController.class);
    private final BsGame bsGame = new BsGame();

    @FXML
    public void handleStartGame(){
        log.trace("handleStartGame called");
        try{
            bsGame.startGame();
        }catch (IllegalFactoryArgument e){
            log.fatal("Wrong argument in Factory cant be handled, Programm will be closed");
            MainMenu.terminateProgramWithMessage(6);
        }
    }
    @FXML
    public void handleConfigureGame(){
        log.trace("handleConfigureGameCalled");
        new ConfigureGameView(bsGame);
    }
}
