package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.BsGame;
import de.hdm_stuttgart.mi.se2.core.grid.BsCoordinate;
import de.hdm_stuttgart.mi.se2.core.players.BsComputerPlayer;
import de.hdm_stuttgart.mi.se2.core.players.IBsPlayer;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class TurnController {
    private static Logger log = LogManager.getLogger(TurnController.class);
    private BsGame bsGame;
    private IBsPlayer player;
    private IBsPlayer opponentPlayer;
    private TurnScene scene;
    private Button[][] buttonsOnFieldLeft;
    private Button[][] buttonsOnFieldRight;
    private List<BsCoordinate> currentTurnAttackedFields;
    private List<BsCoordinate> tillLastTurnAttackedFields = new LinkedList<>();


    public TurnController(Stage primaryStage, BsGame bsGame, IBsPlayer player, IBsPlayer opponentPlayer){
        this.bsGame=bsGame;
        this.player=player;
        this.opponentPlayer=opponentPlayer;
        scene = new TurnScene(primaryStage, player, opponentPlayer);
        buttonsOnFieldLeft = scene.getButtonsOnFieldSelf();
        buttonsOnFieldRight = scene.getButtonsOnFieldOpponent();
        //assign Eventhandler to buttons
        for (Button[] buttonRow: buttonsOnFieldLeft) {
            for(Button button: buttonRow){
                button.setOnAction(actionEvent -> buttonClickedLeft(GridPane.getColumnIndex(button), GridPane.getRowIndex(button)));
            }
        }
        for (Button[] buttonRow: buttonsOnFieldRight) {
            for(Button button: buttonRow){
                button.setOnAction(actionEvent -> buttonClickedRight(GridPane.getColumnIndex(button), GridPane.getRowIndex(button)));
            }
        }
        scene.setEventHandlerForSubmitButton(event -> System.exit(0));
    }
    //If the right player has current Turn the clicked button is disabled, the coordinate is attacked and colored
    private void buttonClickedLeft(int x, int y){
        if(bsGame.getCurrentTurn().equals(opponentPlayer)){
            disableAndColorButton(x,y,player,false);//Color Button gray and disable
                boolean hit = bsGame.placeShot((byte)x, (byte)y);
                if(hit){
                    disableAndColorButton(x,y,player,true);//Color Button red
                    //Change Button Color if ship is dead
                    if(player.getGrid().getShip(new BsCoordinate((byte)x,(byte)y)).isDead()){
                        setButtonColorToDeath(x,y,player, buttonsOnFieldLeft);
                        checkIfGameFinished(opponentPlayer);
                    }
                }else {
                    bsGame.changePlayerTurn();

                }
        }
    }
    //same as above but checks if otherPlayer is computerPlayer
    private void buttonClickedRight(int x, int y){
        if (bsGame.getCurrentTurn().equals(player)){
            disableAndColorButton(x,y,opponentPlayer,false);
            try{
                boolean hit =bsGame.placeShot((byte)x, (byte)y);
                if(hit) {
                    disableAndColorButton(x, y, opponentPlayer, true);
                    if(opponentPlayer.getGrid().getShip(new BsCoordinate((byte)x,(byte)y)).isDead()) {
                        setButtonColorToDeath(x,y,opponentPlayer, buttonsOnFieldRight);
                        checkIfGameFinished(player);
                    }
                }else{
                    bsGame.changePlayerTurn();
                    if(opponentPlayer instanceof BsComputerPlayer){
                        bsGame.placeShot((byte)x, (byte)y);
                        log.debug("ComputerPlayer finished turn");
                        refreshListOfComputerAttacks();
                        for(BsCoordinate coordinate: currentTurnAttackedFields){
                            if(player.getGrid().isOccupied(coordinate)){
                                disableAndColorButton(coordinate, player, true);
                                if(player.getGrid().getShip(coordinate).isDead()){
                                    setButtonColorToDeath(coordinate, player, buttonsOnFieldLeft);
                                    checkIfGameFinished(opponentPlayer);
                                }
                            }else{
                                disableAndColorButton(coordinate, player, false);
                            }
                        }
                        bsGame.changePlayerTurn();
                    }
                }
            }catch (Exception e){
                scene.setErrorLabel("An Error occured please contact developer");

            }
        }
    }
    private void refreshListOfComputerAttacks(){
        List<BsCoordinate> fullList = new LinkedList<>(bsGame.getCopyOfAttackedFieldsFromComputer());//get all attacked bsCoordinate's
        fullList.removeAll(tillLastTurnAttackedFields);//remove all already colored
        tillLastTurnAttackedFields.addAll(fullList); //add new ones to the already colored
        currentTurnAttackedFields=fullList; //set currentTurnAttackedFields to the new bsCoordinate's that need to be colored
        log.debug("List of atacked Coordinates of Computer refreshed");
    }

    private void disableAndColorButton(BsCoordinate coordinate, IBsPlayer player, boolean hit){
        disableAndColorButton(coordinate.x, coordinate.y, player, hit);
    }
    private void disableAndColorButton(int x, int y, IBsPlayer player, boolean hit){
        Button button;
        if(player.equals(this.player)){
            button= buttonsOnFieldLeft[x][y];
        }else{
            button = buttonsOnFieldRight[x][y];
        }
        button.setDisable(true);
        button.getStyleClass().removeAll();
        if(hit){
            button.getStyleClass().add("ButtonHit");
            log.trace("Button ("+x+"|"+y+") colored gray");
        }else{
            button.getStyleClass().add("ButtonClicked");
            log.trace("Button ("+x+"|"+y+") colored light red");

        }
    }
    //wrapper
    private void setButtonColorToDeath(BsCoordinate coordinate, IBsPlayer player, Button[][] buttons){
        setButtonColorToDeath(coordinate.x, coordinate.y, player, buttons);
    }
    //change color of all buttons of one ship to death
    private void setButtonColorToDeath(int x, int y, IBsPlayer player, Button[][] buttons){
        BsCoordinate[] coordinateArray = player.getGrid().getShipLocation(player.getGrid().getShip(new BsCoordinate((byte)x, (byte)y)));
        for(BsCoordinate coordinate:coordinateArray){
                buttons[coordinate.x][coordinate.y].getStyleClass().removeAll();
                buttons[coordinate.x][coordinate.y].getStyleClass().add("ButtonDeadShip");
            log.trace("button color set to death button: "+coordinate.x+"|"+coordinate.y+" on player "+player);

        }
    }
    //check if all ships are death
    private void checkIfGameFinished(IBsPlayer player){
        IBsPlayer oppositePlayer;
        if(player == this.player){
            oppositePlayer= opponentPlayer;
        }else{
            oppositePlayer=this.player;
        }

        log.debug("check if Game Finished, remaining Health: "+ oppositePlayer.getRemainingHealthPoints()+" on player");
        if(oppositePlayer.getRemainingHealthPoints()==0){
            scene.displayWinningStage(player);
        }
    }
}
