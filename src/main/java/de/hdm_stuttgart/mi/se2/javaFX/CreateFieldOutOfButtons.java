package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.players.IBsPlayer;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

/**
 * Creates a GridPane filled with buttons in the size of the grid
 */
 class CreateFieldOutOfButtons {
    private GridPane gridPane;
    private Button[][] buttonsOnField;

     CreateFieldOutOfButtons(IBsPlayer player){
        gridPane= new GridPane();
        buttonsOnField = new Button[player.getGrid().getSize()][player.getGrid().getSize()]; //initiate Array
        for(int x =0; x<player.getGrid().getSize();x++) {
            for (int y = 0; y < player.getGrid().getSize(); y++) {
                buttonsOnField[x][y] = new Button();        // save Button in Array to enable referencing
                buttonsOnField[x][y].getStyleClass().add("fieldButtons");//Add Css Class "fieldButtons to Button
                gridPane.add(buttonsOnField[x][y], x, y);   //add Button to gridPane
            }
        }
    }

     GridPane getGridPane() {
        return gridPane;
    }

     Button[][] getButtonsOnField() {
        return buttonsOnField;
    }
}
