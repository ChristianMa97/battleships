package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.BsGame;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigureGameView extends Application {
    private static Logger log = LogManager.getLogger(ConfigureGameView.class);

    private BsGame bsGame;

    //Constructor
    ConfigureGameView(BsGame bsGame){
        this.bsGame=bsGame;
        start(MainMenu.getPrimaryStage());
    }

    @Override
    public void start(Stage primaryStage){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ConfigureGame.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            ConfigureGameController configureGameController = loader.getController();
            configureGameController.setConfigureGameView(bsGame);
            primaryStage.setTitle("Battleships Configure Game");
            primaryStage.setScene(scene);
            log.trace("show Configure Game View");
            primaryStage.show();


        } catch (Exception e) {
            //Loader couldn't load fxml file
            log.fatal("Exception occurred in Class ConfigureGameView: "+e);
            MainMenu.terminateProgramWithMessage(2);
        }
    }
}
