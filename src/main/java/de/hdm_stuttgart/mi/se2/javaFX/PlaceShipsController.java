package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.BsGame;
import de.hdm_stuttgart.mi.se2.core.grid.BsCoordinate;
import de.hdm_stuttgart.mi.se2.core.players.IBsPlayer;
import de.hdm_stuttgart.mi.se2.core.ships.IBsShip;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class PlaceShipsController {
    private static Logger log = LogManager.getLogger(PlaceShipsController.class);

    private BsGame bsGame;
    private IBsPlayer player;
    private PlaceShipsScene scene;
    private Button[][] buttonsOnField;
    private boolean isVertical = false;
    private List<IBsShip> shipList;
    private IBsShip.ShipType selectedShip = IBsShip.ShipType.Carrier;
    private int countCarrier=0;
    private int countBattleship=0;
    private int countCruiser=0;
    private int countDestroyer=0;

    public PlaceShipsController(Stage primaryStage, BsGame bsGame, IBsPlayer player){
     this.bsGame=bsGame;
     this.player=player;
     scene = new PlaceShipsScene(primaryStage, player);
     this.buttonsOnField=scene.getButtonsOnField();

     //Count number of available Ships per Shiptype
        shipList=player.getGrid().getShipList();
        for (IBsShip ship: shipList) {
            switch(ship.getType()){
                case Carrier:countCarrier++;break;
                case Battleship:countBattleship++;break;
                case Cruiser:countCruiser++;break;
                case Destroyer:countDestroyer++;break;
            }
        }
        //Sets the default values for the Textfields
        scene.setTfCarrier(String.valueOf(countCarrier));
        scene.setTfBattleship(String.valueOf(countBattleship));
        scene.setTfCruiser(String.valueOf(countCruiser));
        scene.setTfDestroyer(String.valueOf(countDestroyer));

     //Siehe SE2 Vorlesungsunterlagen JavaFX-Teil3-EventHandling

        //Eventhandler for buttons
            //long annotation
            scene.setEventHandlerTbHorizontal(new EventHandler<ActionEvent>() {
                                           @Override
                                           public void handle(ActionEvent event) {
                                               isVertical =false;
                                           }
            });
            //lambda expression
            scene.setEventHandlerTbVertical(event -> isVertical =true);
            scene.setEventHandlerTbCarrier(event -> selectedShip= IBsShip.ShipType.Carrier);
            scene.setEventHandlerTbBattleship(event -> selectedShip= IBsShip.ShipType.Battleship);
            scene.setEventHandlerTbCruiser(event -> selectedShip= IBsShip.ShipType.Cruiser);
            scene.setEventHandlerTbDestroyer(event -> selectedShip=IBsShip.ShipType.Destroyer);
            scene.setEventHandlerSubmit(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if(countBattleship==0&&countCarrier==0&&countCruiser==0&&countDestroyer==0){
                        bsGame.submitButtonKlicked();
                    }else{
                        scene.setErrorLabel("Please place all ships");
                    }
                }
            });
            //Set EventHandler for all buttons on field to buttonClicked
        for (Button[] buttonRow:buttonsOnField) {
            for(Button button: buttonRow){
                button.setOnAction(actionEvent -> buttonClicked(GridPane.getColumnIndex(button), GridPane.getRowIndex(button)));
            }
        }

    }

    /**
     * Places a ship at the desired position if it is unsuccessful it will print an error on the errorlabel
     *
     * @param x xPosition of button
     * @param y yPosition of button
     */
    private void buttonClicked(int x, int y){
        if(getCounter(selectedShip)>0) {    //Only if ships of that type left
            try {
                IBsShip ship = getShipOutOfList(selectedShip);  //get ship of that type
                boolean successful = player.getGrid().placeShip(ship, new BsCoordinate((byte) x, (byte) y), isVertical);
                if(successful) {
                    scene.setErrorLabel("");    //clear ErrorLabel
                    log.debug("Ship placed for "+player.getPlayerName()+", Ship:"+ship+" xPosition: "+x+" yPosition: "+y+" Vertical:"+ isVertical);
                    shipList.remove(ship);
                    log.trace("Ship removed from available ships to place.");
                    switch (selectedShip) {  //reduce the number of available ships of the places shipType
                        case Carrier:
                            countCarrier--;
                            scene.setTfCarrier(String.valueOf(countCarrier));
                            break;
                        case Battleship:
                            countBattleship--;
                            scene.setTfBattleship(String.valueOf(countBattleship));
                            break;
                        case Cruiser:
                            countCruiser--;
                            scene.setTfCruiser(String.valueOf(countCruiser));
                            break;
                        case Destroyer:
                            countDestroyer--;
                            scene.setTfDestroyer(String.valueOf(countDestroyer));
                            break;
                    }
                    //color all buttons of the ship
                    BsCoordinate[] coordinaten = player.getGrid().getShipLocation(ship);
                    for(BsCoordinate coordinate: coordinaten){
                        buttonsOnField[coordinate.x][coordinate.y].getStyleClass().removeAll();
                        buttonsOnField[coordinate.x][coordinate.y].getStyleClass().add("placedButtons");

                    }
                }else{
                    scene.setErrorLabel("Placement failed");
                }
            } catch (Exception e) {
                log.warn(e);
                scene.setErrorLabel("Something went wrong with the placement try again");
            }
        }else{
            scene.setErrorLabel("No ships of that type left");
        }

    }
    //returns the first ship of the wanted shipType from available ships to place
    private IBsShip getShipOutOfList(IBsShip.ShipType shipType){

        for(IBsShip ship: shipList){
            if(ship.getType().equals(shipType)){
                log.trace("got ship out of list "+ship);
                return ship;
            }
        }
        log.warn("Ship requested but no of that type left");
        scene.setErrorLabel("An Error occured");
        return null;
    }

    private int getCounter(IBsShip.ShipType shipType){
        switch (shipType){
            case Carrier:return countCarrier;
            case Battleship:return countBattleship;
            case Cruiser:return countCruiser;
            case Destroyer:return countDestroyer;
            default:return -1;
        }
    }




}
