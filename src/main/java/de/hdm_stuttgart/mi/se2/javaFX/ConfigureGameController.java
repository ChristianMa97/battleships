package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.BsGame;
import de.hdm_stuttgart.mi.se2.exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.mi.se2.exceptions.InvalidNumberOfAiPlayer_Exception;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

/**
 * Controller for ConfigureGameView
 */
public class ConfigureGameController {
    private static Logger log = LogManager.getLogger(ConfigureGameController.class);
    /**
     * bsGame: contains Reference to BsGame class
     */
    private BsGame bsGame;


    /**
     * vBoxes: Array witch contains VBoxes to display the Textfield's for the playername's.
     * labels:  related Labels for the Textfield's
     * textAreaError:  TextArea to display Error-Messages for the User
     * vBoxForNames:    VBox that contains the vBoxes
     * choiceBoxAmountPlayer:   ChoiceBox to select the playernumber in total with Ai Player
     * choiceBoxAI:     ChoiceBox to select the number of AI-Player
     * choiceBoxGridSize:   ChoiceBox to select the GridSize
     * ContextMenuAI:   variable ContextMenu of the choiceBoxAI
     * listenToAmountAI: If true, the Listener of choiceBoxAI reacts on value change.
     *                   needed because the Listener also react on the change of available options of the ContextMenu
     */
    @FXML private VBox[] vBoxes;
    @FXML private Label[] labels;
    @FXML private TextField[] textFields;
    @FXML private TextArea textAreaError;
    @FXML private VBox vBoxForNames;
    @FXML private ChoiceBox<Byte> choiceBoxAmountPlayer;
    @FXML private ChoiceBox<Byte> choiceBoxAmountAI;
    @FXML private ChoiceBox<Byte> choiceBoxGridSize;
    private ObservableList ContextMenuAI= FXCollections.observableArrayList();
    private boolean listenToAmountAI=true;

    /**
     * @param bsGame Reference to BsGame
     *
     *  Sets the References and initiate the ChoiceBoxes with default Values.
     *  Also initiate Listeners on the choiceBoxes "AmountAI" and "AmountPlayer"
     */
    void setConfigureGameView(BsGame bsGame){
        this.bsGame=bsGame;
        choiceBoxAmountPlayer.setItems(FXCollections.observableArrayList((byte)2));//ContextMenu for AmountPlayer
        choiceBoxAmountPlayer.setValue((byte)2);//default Value =1
        ContextMenuAI.add((byte)1);
        choiceBoxAmountAI.setItems(ContextMenuAI); //links the ContextMenuAI with the ChoiceBox
        choiceBoxAmountAI.setValue((byte)1); //default Value =1
        choiceBoxGridSize.setItems(FXCollections.observableArrayList((byte)9, (byte)10, (byte)12, (byte)16)); //ContextMenu for GridSize
        choiceBoxGridSize.setValue((byte)9); //default Value =8
        makeNameFields((byte)1); //default 1 Name Fields

        //Listener that updates every time if choiceBoxAmountPlayer changes the value.
        //https://www.thoughtco.com/choicebox-overview-2033928
        final List optionsAmountPlayer = choiceBoxAmountPlayer.getItems();
        choiceBoxAmountPlayer.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                //If the value changes makeNameFields change the number of Textfield's for PlayerName's
                    makeNameFields( (byte) ( (byte)optionsAmountPlayer.get(newValue.byteValue())-choiceBoxAmountAI.getValue() ) );
            }
        });

        //Listener that updates every time if choiceBoxAmountAI changes the value.
        final List optionsAIPlayer = choiceBoxAmountAI.getItems();
        choiceBoxAmountAI.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                if(listenToAmountAI) {  //If the Listener hasn't reacted to the change of available options on the ContextMenu
                    //Same as the Listener from choiceBoxAmountPlayer.
                    makeNameFields( (byte)(choiceBoxAmountPlayer.getValue()-(byte)optionsAIPlayer.get( newValue.intValue() ) ) );
                }
            }
        });

    }

    /**
     * @param amount : Amount of Human Player.
     *
     *  Creates Labels and Textfield's for the Playernames.
     */
    private void makeNameFields(byte amount){
        vBoxForNames.getChildren().clear();

        log.trace("Method: makeNameFields called");
        textFields = new TextField[amount];
        labels = new Label[amount];
        vBoxes = new VBox[amount];

        for(int i=0;i<textFields.length;i++){
            textFields[i]= new TextField("Player "+(i+1));
            labels[i]= new Label("Player Name of Player "+(i+1));
            vBoxes[i]= new VBox();
            System.out.println(textFields[i]+" "+labels[i]);
        }

        for(int i=0;i<textFields.length;i++){
            vBoxes[i].getChildren().add(labels[i]);
            vBoxes[i].getChildren().add(textFields[i]);
            vBoxForNames.getChildren().add(vBoxes[i]);
        }
    }

    /**
     * Updates the ChoiceBox Content every time the Box is clicked.
     * The max Value settable is 1 below the amount of player.
     */
    @FXML
    private void updateContextMenuAI(){
        listenToAmountAI=false;
        log.trace("updateContextMenuAI has been called");
        byte i =choiceBoxAmountPlayer.getValue();
        log.trace("Player amount ="+i);
        ContextMenuAI.removeAll(ContextMenuAI); //clears the ContextMenu
        for(byte b=0; b<i;b++){  //adds the Numbers 0 - (AmountPlayer-1) to the ContextMenu
            ContextMenuAI.add(b);
            log.trace("ContextMenuAI added "+b);
        }
        choiceBoxAmountAI.setItems(ContextMenuAI); //aligns the ContextMenu to the ChoiceBox
        log.trace("assigned to choiceBox");
        listenToAmountAI=true;
    }


    /**
     * Calls configureGame and after that startGame from BsGame.
     */
        @FXML
        private void startGame()throws IllegalFactoryArgument {
        try{
            bsGame.configureGame(choiceBoxAmountPlayer.getValue(), choiceBoxGridSize.getValue(), choiceBoxAmountAI.getValue(), textFields);
            textAreaError.clear();
            textAreaError.setVisible(false);
            bsGame.startGame();
        }catch(InvalidNumberOfAiPlayer_Exception e){
            textAreaError.setText("The number of AI-Player have to be less than the Amount-Of-Player");
            textAreaError.setVisible(true);
        }catch (Exception e){
            textAreaError.setText("An Error occured");
            textAreaError.setVisible(true);
            throw e;
        }
    }


}
