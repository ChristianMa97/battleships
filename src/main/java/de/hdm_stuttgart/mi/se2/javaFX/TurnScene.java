package de.hdm_stuttgart.mi.se2.javaFX;

import de.hdm_stuttgart.mi.se2.core.players.IBsPlayer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class TurnScene extends Application {
    private Stage primaryStage;
    private IBsPlayer player;
    private IBsPlayer opponentPlayer;
    private Button[][] buttonsOnFieldSelf;
    private Button[][] buttonsOnFieldOpponent;
    private GridPane gridPaneSelf;
    private GridPane gridPaneOpponent;
    private Label fieldLabelSelf = new Label("Your Field");
    private Label fieldLabelOpponent= new Label("Opponent's Field");
    private Button submitButton = new Button("submit");
    private Label errorLabel = new Label();



    TurnScene(Stage primaryStage, IBsPlayer player, IBsPlayer opponentPlayer){
        this.primaryStage=primaryStage;
        this.player=player;
        this.opponentPlayer=opponentPlayer;
        start(primaryStage);
    }
    @Override
    public void start(Stage stage){
        BorderPane pane = new BorderPane();
        Scene scene = new Scene(pane);
        scene.getStylesheets().add((getClass().getResource("/css/TurnView.css")).toExternalForm()); //add Stylesheet
        HBox hBox = new HBox();
        hBox.setSpacing(40);
        pane.setCenter(hBox);
        CreateFieldOutOfButtons createFieldOutOfButtonsSelf = new CreateFieldOutOfButtons(player);
        gridPaneSelf= createFieldOutOfButtonsSelf.getGridPane();
        buttonsOnFieldSelf= createFieldOutOfButtonsSelf.getButtonsOnField();
        CreateFieldOutOfButtons createFieldOutOfButtonsOpponent = new CreateFieldOutOfButtons(opponentPlayer);
        gridPaneOpponent= createFieldOutOfButtonsOpponent.getGridPane();
        buttonsOnFieldOpponent= createFieldOutOfButtonsOpponent.getButtonsOnField();
        VBox vBoxSelf = new VBox();
        VBox vBoxOpponent = new VBox();
        hBox.getChildren().add(vBoxSelf);
        hBox.getChildren().add(vBoxOpponent);
        fieldLabelOpponent.getStyleClass().add("FieldLabel");
        fieldLabelSelf.getStyleClass().add("FieldLabel");
        vBoxSelf.getChildren().add(fieldLabelSelf);
        vBoxSelf.getChildren().add(gridPaneSelf);
        vBoxOpponent.getChildren().add(fieldLabelOpponent);
        vBoxOpponent.getChildren().add(gridPaneOpponent);
        pane.setBottom(errorLabel);


        primaryStage.setTitle("Turn of Player "+player.getPlayerName());
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    void displayWinningStage(IBsPlayer player){
        AnchorPane pane = new AnchorPane();
        Scene scene = new Scene(pane);
        VBox vBox = new VBox();
        pane.getChildren().add(vBox);
        vBox.setPrefWidth(500);
        vBox.setPrefHeight(300);
        vBox.setAlignment(Pos.CENTER);
        Label label = new Label("Player "+ player.getPlayerName()+" has won!");
        Stage winningStage = new Stage();
        submitButton.setPrefHeight(50);
        submitButton.setPrefWidth(100);
        label.setPrefWidth(500);
        label.setPrefHeight(50);
        label.setTextAlignment(TextAlignment.CENTER);
        label.setStyle("-fx-font-size: 1.5em");
        vBox.getChildren().addAll(label, submitButton);
        winningStage.setMinWidth(500);
        winningStage.setMinHeight(300);
        winningStage.setTitle("Game Finished");
        winningStage.setScene(scene);
        winningStage.show();

    }

    void setEventHandlerForSubmitButton(EventHandler e){
        submitButton.setOnAction(e);
    }
     void setErrorLabel(String e){
        errorLabel.setText(e);
    }
     Button[][] getButtonsOnFieldSelf(){
        return buttonsOnFieldSelf;
    }
     Button[][] getButtonsOnFieldOpponent(){
        return buttonsOnFieldOpponent;
    }
}

