module javaFX {                     //package-Name
    requires javafx.controls;
    requires javafx.fxml;
    requires log4j.api;
    opens de.hdm_stuttgart.mi.se2.javaFX to javafx.fxml, log4j.api;
    exports de.hdm_stuttgart.mi.se2.javaFX;
}