# SE2 Projekt: Battleships

## Members:
- Lenk Valentin @vl030 
- Merzhäuser Elias @em059 
- Martin Christian @cm132

### Project outline can be found in file SE2_Dokumentation.pdf

The project is a typical ship sinking game. It is possible to play man against man or man against computer.<br>
The graphics are realized in JavaFX<br>
The Dokumentation can be found in SE2_Dokumentation.pdf